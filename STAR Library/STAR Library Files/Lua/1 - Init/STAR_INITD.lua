// SONIC ROBO BLAST 2
// STAR Library
//-----------------------------------------------------------------------------
// Made by Star "Guy Who Names Scripts After Him" ManiaKG.
//-----------------------------------------------------------------------------
/// \file  STAR_INIT.lua
/// \brief Initializes STAR data

// ------------------------ //
//        Variables
// ------------------------ //

if not (STAR)
	rawset(_G, "STAR", {});
end